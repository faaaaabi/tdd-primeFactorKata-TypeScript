export default function primeFactors(n: number): Array<number> {
    const factors: Array<number> = [];
    let remainder: number = n;
    let divisor: number = 2;
    while (remainder > 1) {
        while (remainder % divisor === 0) {
            factors.push(divisor);
            remainder /= divisor;
        }
        divisor++;
    }
    return(factors)
}