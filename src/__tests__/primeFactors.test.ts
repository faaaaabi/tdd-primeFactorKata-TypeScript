import primeFactors  from '../primeFactors'

describe('Testing for valid prime factors of an given integer', () => {
    test('should return an empty array for 1 as input', () => {
        expect(primeFactors(1)).toEqual([]);
    });
    test('should return an array: [2] for 2 as input', () => {
        expect(primeFactors(2)).toEqual([2]);
    });
    test('should return an array: [3] for 3 as input', () => {
        expect(primeFactors(3)).toEqual([3]);
    });
    test('should return an array: [2,2] for 4 as input', () => {
        expect(primeFactors(4)).toEqual([2,2]);
    });
    test('should return an array: [5] for 5 as input', () => {
        expect(primeFactors(5)).toEqual([5]);
    });
    test('should return an array: [2,3] for 6 as input', () => {
        expect(primeFactors(6)).toEqual([2,3]);
    });
    test('should return an array: [7] for 7 as input', () => {
        expect(primeFactors(7)).toEqual([7]);
    });
    test('should return an array: [2,2,2] for 8 as input', () => {
        expect(primeFactors(8)).toEqual([2,2,2]);
    });
    test('should return an array: [3,3] for 9 as input', () => {
        expect(primeFactors(9)).toEqual([3,3]);
    });
    test('should return an array: [2,2,3,3,5,7,11,11,13] for 2*2*3*3*5*7*11*11*13 as input', () => {
        expect(primeFactors(2*2*3*3*5*7*11*11*13)).toEqual([2,2,3,3,5,7,11,11,13]);
    });
});
