# Prime Factor Kata (TypeScript)

This is a TypeScript implementation of Robert C. Martins "Prime Factor Kata"

### Installing

```bash
$ npm install
``` 

### Testing
```bash
$ npm test
``` 